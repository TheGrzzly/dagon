import React, {Suspense} from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Modal from './components/UI/Modal/Modal';
import Header from './components/Header/Header';
import Spinner from './components/UI/Spinner/Spinner';

const Characters = React.lazy(() => import('./components/Characters/Characters'));
const Factions = React.lazy(() => import('./components/Factions/Factions'));
const Inventory = React.lazy(() => import('./components/Inventories/Inventory'));
const Landing = React.lazy(()=>import('./components/UI/Landing/Landing'));
const NotFound = React.lazy(() => import('./components/UI/NotFound/Notfound'));

function App() {
  return (
    <>  
        <Header/>
          <Suspense fallback={<Spinner />}>
            <Switch>
              <Route path="/" exact ><Landing /></Route>
              <Route path="/health" exact><h3>Successful Death Save</h3></Route>
              <Route path="/factions"><Factions /></Route>
              <Route path="/characters"><Characters /></Route>
              <Route path="/inventory" exact> <Redirect to="/inventory/1" /></Route>
              <Route path="/inventory/:idInventory"><Inventory /></Route>
              <Route path="*"><NotFound /></Route>
            </Switch>
          </Suspense >
        <Modal />
    </>
  );
}

export default App;
