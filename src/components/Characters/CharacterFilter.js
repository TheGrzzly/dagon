
const CharacterFilter = (props) => {
    const filterChangeHandler = (event) => {
        props.onChangeFilter(event.target.value);
    };

    return (
        <div className="ui container">
            <div className="ui right aligned">
                <span className="ui">Filter by Level:  </span>
                <select value={props.selected} className="ui dropdown" onChange={filterChangeHandler}>
                    <option value="all">all</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>
        </div>
    );  
};

export default CharacterFilter;