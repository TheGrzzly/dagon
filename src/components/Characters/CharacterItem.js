import { useHistory, Link } from "react-router-dom"

const CharacterItem = (props) => {
    const history = useHistory();

    const viewInventoryHandler = (id) =>{
        console.log('Loading inventory for '+ id);
        history.push('/inventory/'+id);
    }

    const calculateDaysSinceLastPlayed = (last_played) => {
        var last_date = new Date(last_played);
        var date = new Date();
        var dif_in_time = date.getTime() - last_date.getTime();
        var dif_in_dates = dif_in_time / (1000 * 3600 * 24);
        return Math.floor(dif_in_dates);
    }

    let days_left = calculateDaysSinceLastPlayed(props.last_played);

    return (
        <div className="ui card raised">
            <div className="content">
                <Link className="header" to={`/inventory/${props.id}`} onClick={viewInventoryHandler}>{props.name}</Link>
                <div className="meta">
                    <span className="right floated last-played">Last played {days_left} day(s) ago</span>
                    <span className="id"><strong>Id: </strong>{props.id}</span>
                </div>
                <div className="description">
                    <p><strong>Race: </strong>{props.race}</p>
                    <p><strong>Faction: </strong>{props.faction}</p>
                    <p><strong>Class: </strong>{props.class}</p>
                    <p><strong>Level: </strong>{props.level}</p>
                    <p><strong>Exp: </strong>{props.exp}</p>
                    <p><strong>Last Played: </strong>{props.last_played}</p>
                </div>
            </div>
            <div className="ui bottom attached button" onClick={() => viewInventoryHandler(props.id)}>
                <i className="suitcase icon"></i>
                View Inventory
            </div>
        </div>
    );
}

export default CharacterItem;