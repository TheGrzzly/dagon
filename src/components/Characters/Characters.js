import { useState, useEffect, useContext } from "react";
import { useHistory } from 'react-router-dom'
import CharacterFilter from "./CharacterFilter";
import CharacterItem from "./CharacterItem";
import Grid from "../UI/Grid/Grid";
import ErrorContext from "../../store/error-context";
import Placeholder from "../UI/Placeholder/Placeholder";

const Characters = () => {
    const history = useHistory();
    const errCtx = useContext(ErrorContext);
    const [characters, setCharacters] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [filteredLevel, setFilteredLevel] = useState('all');

    useEffect(() => {
        document.title='Characters';
        setIsLoading(true);
        fetch(process.env.REACT_APP_API_URL+':'+process.env.REACT_APP_API_PORT+'/characters').then(response => {
            if (!response.ok){
                if (response.status === 404){
                    history.push('/');
                }
                throw new Error(response.statusText);
            }
            return response.json();
        }).then(data => {
            console.log(data);
            setCharacters(data);
            setIsLoading(false);
        }).catch(err => {
            console.log(err);
            errCtx.onErrorChange({
                title: err.name,
                message: err.message,
                isActive: true,
            });
        })
    }, [])

    const filterChangeHandler = (selectedLevel) => {
        setFilteredLevel(selectedLevel);
    };

    const filteredCharacters = characters.filter(character => {
        if (filteredLevel === 'all'){
            return true
        }

        return character.level === parseInt(filteredLevel);
    });

    let chars = (
        <div className="ui center aligned container">
            <p className="ui header">No Character was found</p>
        </div>
    );

    if (filteredCharacters.length > 0) {
        chars = filteredCharacters.map(character => (
            <CharacterItem
                key={character.id}
                id={character.id}
                name={character.name}
                race={character.race}
                faction={character.faction}
                class={character.class}
                level={character.level}
                exp={character.exp}
                last_played={character.last_played}
            />
        ))
    }

    if (isLoading) {
        chars = <Placeholder />
    }

    return (
        <>  
            <div className="ui left aligned container">
                <h2 className="ui header">Characters</h2>
            </div>
            <div className="ui hidden divider" />
            <div className="ui justified container">
                <CharacterFilter onChangeFilter={filterChangeHandler} selected={filteredLevel} />
            </div>
            <Grid >
                {chars}
            </Grid>
        </>
    )
}

export default Characters;