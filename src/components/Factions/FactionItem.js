const FactionItem = (props) => {
    return (
        <div className="ui card raised">
            <div className="content">
                <div className="header">{props.name}</div>
                <div className="meta">
                    <span className="right floated id"><strong>Id: </strong>{props.id}</span>
                </div>
                <div className="description">Cap of <strong>{props.cap}</strong></div>
            </div>
        </div>
    );
}

export default FactionItem;