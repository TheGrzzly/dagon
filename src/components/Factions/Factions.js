import { useEffect, useState, useContext } from "react";
import { useHistory } from 'react-router-dom'

import FactionItem from "./FactionItem";
import Grid from "../UI/Grid/Grid";
import Placeholder from "../UI/Placeholder/Placeholder";
import ErrorContext from "../../store/error-context";

const Factions = () => {
    const history = useHistory();
    const errCtx = useContext(ErrorContext);
    const [factions, setFactions] = useState([]);
    
    useEffect(() => {
        document.title = 'Factions';
        fetch(process.env.REACT_APP_API_URL+':'+process.env.REACT_APP_API_PORT+'/factions').then(response => {
            if (!response.ok){
                switch(response.status){
                    case 404:
                        history.push('/');
                        break;
                    case 500:
                        throw new Error(response.statusText);
                    default:
                        throw new Error('Something went wrong!');
                }
            }
            return response.json();
        }).then(data => {
            console.log(data);
            setFactions(data);
        }).catch(error => {
            errCtx.onErrorChange({
                title: error.name,
                message: error.message,
                isActive: true,
            })
            console.log(error.name);
        });
    }, []);

    let factionObj = <Placeholder />

    if (factions.length > 0) {
        factionObj = factions.map(faction => (
            <FactionItem
                key={faction.id}
                id={faction.id}
                name={faction.name}
                cap={faction.cap}
            />
        ))
    }

    return (
        <>
            <div className="ui left aligned container">
                <h2 className="ui header">Factions</h2>
            </div>
            <Grid >
                {factionObj}
            </Grid>
        </>
    );
}

export default Factions;