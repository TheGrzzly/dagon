import { NavLink, useHistory } from 'react-router-dom';
import andoraLogo from '../../assets/andora_logo.png'
import './Header.css'

const Header = () => {
    const history = useHistory();

    const homeClickHandler = () => {
        history.push('/');
    }

    return (
        <div className="ui top secondary pointing menu">
            <div className="item logoimg">
                <img src={andoraLogo} alt="Andora" onClick={homeClickHandler}/>
            </div>
            <div className="right menu headerurls">
                <NavLink to="/factions" activeClassName='active' className='item'>
                    Factions
                </NavLink>
                <NavLink to="/characters" activeClassName="active" className='item'>
                    Characters
                </NavLink>
                <NavLink to="/inventory" activeClassName="active" className='item'>
                    Inventories
                </NavLink>
            </div>
        </div>
    );
};

export default Header;