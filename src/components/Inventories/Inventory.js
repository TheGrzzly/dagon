import { useContext, useEffect, useState } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';

import InventoryItem from "./InventoryItem";
import Grid from "../UI/Grid/Grid";
import Spinner from "../UI/Spinner/Spinner";
import ErrorContext from '../../store/error-context';

const Inventory = () => {
    const history = useHistory();
    const params = useParams();
    const errCtx = useContext(ErrorContext);
    const [id, setId] = useState('1');
    const [name, setName] = useState('');
    const [inventory, setInventory] = useState(null);

    console.log(params);

    useEffect(() => {
        console.log(params.idInventory);
        setId(params.idInventory);
    }, [params.idInventory]);

    useEffect( () => {
        async function fetchName(id){
            try{
                const response = await fetch(process.env.REACT_APP_API_URL+':'+process.env.REACT_APP_API_PORT+'/characters?id='+id)
                if (!response.ok) {
                    if (response.status === 404){
                        history.push('/not_found');
                    }
                    const errMessage = await response.text();
                    throw new Error(errMessage);
                }
                const data = await response.json();
                console.log(data.name);
                document.title = 'Inventory '+data.name;
                setName(data.name);
            }catch (error) {
                console.log(error);
    
                errCtx.onErrorChange({
                    title: error.name,
                    message: error.message,
                    isActive: true,
                });
            }
        }
        fetchName(id);
    }, [id]);

    useEffect(() => {
        async function fetchInventory(id){
            try{
                const response = await fetch(process.env.REACT_APP_API_URL+':'+process.env.REACT_APP_API_PORT+'/inventory?id='+id)
                if (!response.ok){
                    if (response.status === 404){
                        history.push('/not_found');
                    }
                    const errMessage = await response.text();
                    throw new Error(errMessage);
                }
                const data = await response.json();
                console.log(data);
                setInventory(data);
            }catch (error){
                console.log(error);
    
                errCtx.onErrorChange({
                    title: error.name,
                    message: error.message,
                    isActive: true,
                });
            }
        }
        fetchInventory(id);
    }, [id]);

    let inv = <Spinner />

    let breadcrumb = <Spinner />

    if (name !== ''){
        breadcrumb  = ( <div className="ui container">
                            <div className="ui large breadcrumb">
                                <Link to="/" className="section">Home</Link>
                                <div className="divider"> / </div>
                                <Link to="/characters" className="section">Characters</Link>
                                <div className="divider"> / </div>
                                <div className="active section">{name}</div>
                            </div>
                        </div>)
    }

    if (inventory && name !== ''){
        let item_obj = inventory.items.map(item => (
            <InventoryItem 
                key={item.name}
                name={item.name}
                description={item.description}
                quantity={item.quantity}
            />
        ))
    
        let magic_item_obj = inventory.magical_items.map(item => (
            <InventoryItem 
                key={item.name}
                name={item.name}
                description={item.description}
                quantity={item.quantity}
            />
        ))

        let bank_item_obj = inventory.bank.items.map(item => (
            <InventoryItem 
                key={item.name}
                name={item.name}
                description={item.description}
                quantity={item.quantity}
            />
        ))

        let bank_magic_item_obj = inventory.bank.magical_items.map(item => (
            <InventoryItem 
                key={item.name}
                name={item.name}
                description={item.description}
                quantity={item.quantity}
            />
        ))


        inv =   <div className="ui container padded raised segment">
                    <h2 className="ui header">
                        Inventory: {name}
                        <div className="ui right floated sub header">
                            <i className="bitcoin icon"></i>
                            <strong>Gold:</strong> {inventory.gold}
                        </div>
                        <div className="ui right floated sub header">
                            <strong>Bank gold:</strong> {inventory.bank.gold}
                        </div>
                        <div className="sub header"><strong>ID:</strong> {params.idInventory}</div>
                    </h2>
                    <h3 className="ui horizontal divider header">
                        <i className="suitcase icon"></i>
                        Items
                    </h3>
                    <Grid>
                        {item_obj}
                    </Grid>
                    <h3 className="ui horizontal divider header">
                        <i className="magic icon"></i>
                        Magical Items
                    </h3>
                    <Grid>
                        {magic_item_obj}
                    </Grid>
                    <h3 className="ui horizontal divider header">
                        <i className="suitcase icon"></i>
                        Bank Items
                    </h3>
                    <Grid>
                        {bank_item_obj}
                    </Grid>
                    <h3 className="ui horizontal divider header">
                        <i className="magic icon"></i>
                        Bank Magical Items
                    </h3>
                    <Grid>
                        {bank_magic_item_obj}
                    </Grid>
                </div>
    }

    return (
        <>
            {breadcrumb}
            {inv}
        </>
    )
}

export default Inventory;