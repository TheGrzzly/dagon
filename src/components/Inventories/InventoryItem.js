const InventoryItem = (props) => {
    return(
        <div className="ui card raised">
            <div className="content">
                <div className="header">{props.name}</div>
                <div className="meta">
                    <span className="right floated quantity">{props.quantity}</span>
                </div>
                <div className="description">{props.description}</div>
            </div>
        </div>
    )
}

export default InventoryItem;