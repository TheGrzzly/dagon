import React, { useReducer, useContext, useRef } from 'react';
import ErrorContext from '../../store/error-context';
import Input from '../UI/input/Input';

const inputReducer = (state, action) => {
    switch (action.type){
        case 'ID_INPUT':
            if (action.val == null){
                return {
                    ...state,
                    idValue: action.val,
                    idValid: false,
                }
            }

            if (+action.val < 1){
                return {
                    ...state,
                    idValue: action.val,
                    idValid: false,
                }
            }

            return {
                ...state,
                idValue: action.val,
                idValid: true,
            }
        case 'NAME_INPUT':
            if (action.val.trim().length < 1){
                return {
                    ...state,
                    nameValue: action.val,
                    nameValid: false,
                }
            }

            return {
                ...state,
                nameValue: action.val,
                nameValid: true,
            }
        case 'CAP_INPUT':
            if (action.val == null){
                return {
                    ...state,
                    capValue: action.val,
                    capValid: false,
                }
            }

            if (+action.val < 10){
                return {
                    ...state,
                    capValue: action.val,
                    capValid: false,
                }
            }

            return {
                ...state,
                capValue: action.val,
                capValid: true,
            }
        default:
    }


    return {
        idValue: null,
        idValid: true,
        nameValue: '',
        nameValid: true,
        capValue: null,
        capValid: true,
    };
};

const FactionForm = (props) => {
    const ctx = useContext(ErrorContext);

    const idInputRef = useRef();
    const nameInputRef = useRef();
    const capInputRef = useRef();

    const [inputState, dispatchInput] = useReducer(inputReducer, {
        idValue: null,
        idValid: true,
        nameValue: '',
        nameValid: true,
        capValue: null,
        capValid: true,
    },);

    const idChangeHandler = (event) =>{
        dispatchInput({type: 'ID_INPUT', val: event.target.value});
    }

    const nameChangeHandler = (event) => {
        dispatchInput({type: 'NAME_INPUT', val: event.target.value});
    }

    const capChangeHandler = (event) => {
        dispatchInput({type: 'CAP_INPUT', val: event.target.value});
    }

    const submitHandler = (event) => {
        event.preventDefault();
        let inputObj = {
            ...inputState
        }

        inputObj.idValid = inputObj.idValue == null? false : true;
        inputObj.nameValid = inputObj.nameValue.trim().length === 0 ? false : true;
        inputObj.capValid = inputObj.capValue == null ? false : true;

        if (!inputObj.idValid || !inputObj.nameValid || !inputObj.capValid){
            // setIsValid(validationObj);
            const error = {
                title: 'Invalid Values',
                message: 'Please enter valid values',
                isActive: true,
            }
            console.log(props);
            ctx.onErrorChange(error);
            nameInputRef.current.focus();
            return;
        }

        if (+inputObj.idValue < 1){
            const error = {
                title: 'Invalid ID',
                message: "Please enter an id that's bigger than 0",
                isActive: true,
            }
            ctx.onErrorChange(error);
            idInputRef.current.focus();
            return;
        }

        if (+inputObj.capValue < 10){
            const error = {
                title: 'Invalid Cap',
                message: "Please enter a cap that's bigger than 10",
                isActive: true,
            }
            ctx.onErrorChange(error);
            capInputRef.current.focus();
            return;
        }

        let factionData = {
            id: inputObj.idValue,
            name: inputObj.nameValue,
            cap: inputObj.capValue,
        }

        props.onFactionSubmitForm(factionData);

        dispatchInput({type: 'RESET'});
        props.onFinish(false);
    }



    return (
        <div className="ui form">
                <Input 
                    ref={idInputRef}
                    className='required field' 
                    valid={inputState.idValid}
                    label='Id:'
                    type='number'
                    placeholder='1'
                    min='1'
                    step='1'
                    value={inputState.idValue}
                    onChange={idChangeHandler} />
                <Input 
                    ref={nameInputRef}
                    className='required field'
                    valid={inputState.nameValid} 
                    label='Name'
                    type='text'
                    placeholder='Teca Trailblazers'
                    value={inputState.nameValue}
                    onChange={nameChangeHandler}/>
                <Input 
                    ref={capInputRef}
                    className='required field'
                    valid={inputState.capValid}
                    label='Member Cap'
                    type='number'
                    placeholder='10'
                    min='10'
                    step='1'
                    value={inputState.capValue}
                    onChange={capChangeHandler} />
            <div className="ui secondary button" onClick={() => props.onFinish(false)}>Cancel</div>
            <div className="ui submit button" onClick={submitHandler}>Submit</div>
        </div>
    )
}

export default FactionForm;