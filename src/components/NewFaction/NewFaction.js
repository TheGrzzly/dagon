import { useState, useEffect } from "react";
import FactionForm from "./FactionForm"

const NewFaction = (props) => {

    const [isEditing, setIsEditing] = useState(false);

    useEffect(() => {
        const storedIsEditing = localStorage.getItem('isEditing');

        if (storedIsEditing === '1') {
            setIsEditing(true);
        }
    }, []);

    const submitFactionFormHandler = (FactionFormData) => {
        props.onFactionAddItem(FactionFormData);
    }

    const setEditingHandler = (state) => {
        localStorage.setItem('isEditing', state ? '1' : '0');
        setIsEditing(state);
    };

    let form = (
        <div className="ui center aligned container">
            <button className="big ui button" onClick={() => setEditingHandler(true)}>
                Show Form
            </button>
        </div>
    );

    if (isEditing){
        form = <FactionForm onFinish={setEditingHandler} onFactionSubmitForm={submitFactionFormHandler}/>
    }

    return (
        <div className="ui container padded raised segment">
            {form}
        </div>
    )
}

export default NewFaction;