import { useEffect, useState } from "react";

const Accordion = (props) => {

    const [activeItem, setActiveItem] = useState([]);
    
    useEffect(() => {
        let items = new Array(props.items.length).fill(false);
        setActiveItem(items);
        console.log(props.items);

    }, [props.items])

    const itemClickHandler = (id) => {
        let items = new Array(props.items.length).fill(false);
        items[id] = true;
        setActiveItem(state => {
            items[id] = !state[id];
            return items;
        });
    }

    let items = props.items.map(item => (
        <>
            <div className={`title ${activeItem[item.id] ? 'active': ''}`} key={item.id} onClick={() => itemClickHandler(item.id)}>
                <i className="dropdown icon" />
                {item.title}
            </div>
            <div className={`content ${activeItem[item.id] ? 'active' : ''}`}>
                {item.content}
            </div>
        </>
    ))

    console.log(items);

    return (
        <div className="ui accordion">
            {items}
        </div>
    )
};

export default Accordion;