const Grid = (props) => {
    return (
        <div className="ui four cards doubling stackable grid container items">
            {props.children}
        </div>
    )
};

export default Grid;