import { useEffect } from "react";
import ListItem from "../List/ListItem";
import andoraMap from '../../../assets/andora_map.jpeg';
import Accordion from "../Accordion/Accordion";

const Landing = () => {

    useEffect(() => {
        document.title = 'Dagon';
    }, []);

    return (
        <>
            <h1 className="ui center aligned icon header">
                Welcome to Andora
            </h1>

            
            <img className="ui centered huge image" src={andoraMap} alt="Andora Map" />

            <div className="ui hidden divider" />

            <div className="ui text container">
                <div className="ui list">
                    <ListItem 
                        icon="file"
                        header="Character creation rules"
                        url="/"
                    />
                    <ListItem  
                        icon="file"
                        header="Andora rules"
                        url="/"
                    />
                    <ListItem 
                        icon="file"
                        header="Regions"
                        url="/"
                    />
                    <ListItem 
                        header="Andora Wiki"
                        icon="file"
                        url="https://andora.fandom.com/wiki/Andora_Wiki"
                        external={true}
                    />
                </div>
            </div>
            
        </>
    );
};

export default Landing;