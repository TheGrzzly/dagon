import { Link } from "react-router-dom";

const ListItem = (props) => {
    let icon = null;

    if (props.icon) {
        icon = <i className={`${props.icon} icon`}/>
    }

    let description = null;

    let url = <Link className="item" to={props.url}>{props.header}</Link>

    if (props.external) {
        url = <a className="item" href={props.url} target="_blank">{props.header}</a>
    }

    description = (
        <div className="content">
        {url}
        </div>
    )

    return (
        <div className="item">
            {icon}
            {description}
        </div>
    );
}

export default ListItem;