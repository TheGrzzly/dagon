import { useContext } from 'react';
import './Modal.css'

import ErrorContext from '../../../store/error-context';
import ReactDom from 'react-dom';

const Modal = () => {
    const ctx = useContext(ErrorContext);

    const changeIsActive = () =>{
        const error = {
            title: '',
            message: '',
            isActive: false,
        }
        ctx.onErrorChange(error);
    }

    return (
        <div className={`ui dimmer modals page ${ctx.isActive ? 'visible active alert-dimmer' : 'hidden'}`}>
            <div className={`ui modal tiny ${ctx.isActive ? 'visible active alert-Modal' : 'hidden'}`} 
                ref={(el) => {
                    if (el){
                        el.style.setProperty('display', 'block', 'important');
                    }
                }}
            >
                <div className="header">{ctx.title}</div>
                <div className="content">
                    <p>{ctx.message}</p>
                </div>
                <div className="actions">
                    <div className="ui button" onClick={changeIsActive}>Close</div>
                </div>
            </div>
        </div>
    );
};

const ModalContainer = () => {
    return (
        <>
            {ReactDom.createPortal(<Modal />, document.getElementById('modal-root'))}
        </>
    );
}

export default ModalContainer;