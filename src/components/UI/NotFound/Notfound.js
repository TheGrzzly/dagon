import { useHistory } from "react-router-dom";
import { useEffect } from "react";
import notFoundImage from '../../../assets/Pollo.png'

const NotFound = () => {
    const history = useHistory();

    useEffect(() => {
        document.title = 'Not Found, Pollo';
        setTimeout(()=> {
            history.push("/");
        }, 10000);
    }, [])

    return (
        <>
            <h1 className="ui center aligned header">
                <img className="ui image" src={notFoundImage} alt="Pollo thumbsUp" />
                Not Found, Ask Grizz for help
            </h1>
        </>
    )
}

export default NotFound;