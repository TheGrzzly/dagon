const Placeholder = () => {
    let letArray = [1,2,3,4];

    let elements = letArray.map(num => 
        <div key={num} className="column">
            <div className="ui raised segment">
                <div className="ui placeholder">
                    <div className="image header">
                        <div className="line"></div>
                        <div className="line"></div>
                    </div>
                    <div className="paragraph">
                        <div className="medium line"></div>
                        <div className="short line"></div>
                    </div>
                </div>
            </div>
        </div>
    )

    return(
        <div className="ui four column doubling stackable grid container items">
            {elements}
        </div>
    )
}

export default Placeholder;