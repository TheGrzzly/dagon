import React from 'react';

const spinner = () => (
    <div className="ui active centered inline loader"></div>
);

export default spinner