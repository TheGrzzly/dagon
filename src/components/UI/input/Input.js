import React, { useRef, useImperativeHandle } from "react";


const Input = React.forwardRef((props, ref) => {
    const inputRef = useRef();

    const focus = () => {
        inputRef.current.focus();
    };

    useImperativeHandle(ref, () => {
        return {
            focus: focus,
        };
    });
    
    return (
        <div className={`${props.className} ${!props.valid && 'error'}`}>
            <label>{props.label}</label>
            <input ref={inputRef} type={props.type} value={props.value} placeholder={props.placeholder} onChange={props.onChange} min={props.min} max={props.max} step={props.step} />
        </div>
    )
});

export default Input;