import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import App from './App';
import { ErrorContextProvider } from './store/error-context' 
import { BrowserRouter } from 'react-router-dom'

ReactDOM.render(
    <BrowserRouter>
        <ErrorContextProvider>
            <App />
        </ErrorContextProvider>
    </BrowserRouter>,
    document.getElementById('root')
);