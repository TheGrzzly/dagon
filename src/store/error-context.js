import React, {useState} from 'react';

const ErrorContext = React.createContext({
    title: '',
    message: '',
    isActive: false,
    onErrorChange: (error) => {}
});

export const ErrorContextProvider = (props) => {
    const [error, setError] = useState({
        title: '',
        message: '',
        isActive: false,
    });

    const errorChangeHandler = (error) =>{
        setError(error);
    }

    return <ErrorContext.Provider value={{
        title: error.title,
        message: error.message,
        isActive: error.isActive,
        onErrorChange: errorChangeHandler}}
    >{props.children}</ErrorContext.Provider>
};

export default ErrorContext;